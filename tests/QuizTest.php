<?php

use Reciclare\Adapter\GamificationAdapter;
use Reciclare\Gamification\Quiz;

class QuizTest extends PHPUnit_Framework_TestCase
{
    protected $accessToken;
    protected $baseUrl = 'http://localhost:8000';
    protected $adapter;
    protected $cpf;

    public function setUp()
    {
        $this->setUpAdapter();
        $this->cpf = '45487975809';
    }

    public function setUpAdapter()
    {
        $clientId = 2;
        $clientSecret = 'T45Ag1sjaCDnB5kPCd8CahCzciAuVXIvmWhH0gIq';

        $adapter = new GamificationAdapter($this->baseUrl);
        $adapter->setClientId($clientId);
        $adapter->setClientSecret($clientSecret);
        $this->adapter = $adapter;
    }

    public function testQuizStartEventSuccess()
    {
        $quiz = new Quiz($this->adapter);
        $result = $quiz->start($this->cpf);
        $this->assertTrue($result);
    }

    public function testQuizRightAnswer()
    {
        $quiz = new Quiz($this->adapter);
        $result = $quiz->rightAnswer($this->cpf);
        $this->assertTrue($result);
    }

    public function testQuizFinish()
    {
        $quiz = new Quiz($this->adapter);
        $result = $quiz->finish($this->cpf);
        $this->assertTrue($result);
    }

    public function testQuizStartEventError()
    {
        $quiz = new Quiz($this->adapter);
        $result = $quiz->startClassQuiz($this->cpf);
        $this->assertFalse($result);
    }
}
