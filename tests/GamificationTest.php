<?php

use Reciclare\Adapter\GamificationAdapter;
use Reciclare\Gamification\Productivity;
use Reciclare\Http\GamificationRequest;

class GamificationTest extends PHPUnit_Framework_TestCase
{
    protected $accessToken;
    protected $baseUrl = 'http://localhost:8000';
    protected $adapter;

    public function setUp()
    {
        $this->setUpAdapter();
        $this->getCredentials();
    }

    public function setUpAdapter()
    {
        $clientId = 2;
        $clientSecret = 'T45Ag1sjaCDnB5kPCd8CahCzciAuVXIvmWhH0gIq';

        $adapter = new GamificationAdapter($this->baseUrl);
        $adapter->setClientId($clientId);
        $adapter->setClientSecret($clientSecret);
        $this->adapter = $adapter;
    }

    public function getCredentials()
    {
        $request = new GamificationRequest;
        $request->setAdapter($this->adapter);
        $request->credentials();
        $this->accessToken = $request->getAccessToken();
    }

    public function testProductivity()
    {
        $cpf = '48058193836';
        $accessToken = $this->accessToken;

        $productivity = new Productivity($this->adapter);
        $productivity->send($cpf, 90, 100);
    }
}
