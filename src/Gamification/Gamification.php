<?php

namespace Reciclare\Gamification;

use Reciclare\Adapter\GamificableHttpAdapter;
use Reciclare\Http\GamificationRequest;
use Reciclare\Util;

class Gamification
{
    protected $request;

    public function __construct(GamificableHttpAdapter $adapter)
    {
        $this->setUpRequest($adapter);
    }

    public function setUpRequest($adapter)
    {
        $request = new GamificationRequest;
        $request->setAdapter($adapter);
        $request->credentials();
        $this->request = $request;
    }

    public function getClass()
    {
        $class = explode('\\', get_class($this));
        $class = array_pop($class);
        return $class;
    }

    public function __call($name, $arguments)
    {
        $cpf = $arguments[0];
        $class = Util::snakeCase($this->getClass(), '-');
        $method = Util::snakeCase($name, '-');
        $alias = $class . '-' . $method;
        $result = $this->request->event($alias, $cpf);
        return $result;
    }
}
