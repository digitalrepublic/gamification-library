<?php

use Reciclare\Adapter\GamificationAdapter;
use Reciclare\Gamification\Content;

class ContentTest extends PHPUnit_Framework_TestCase
{
    protected $accessToken;
    protected $baseUrl = 'http://localhost:8000';
    protected $adapter;
    protected $cpf;

    public function setUp()
    {
        $this->setUpAdapter();
        $this->cpf = '45487975809';
    }

    public function setUpAdapter()
    {
        $clientId = 2;
        $clientSecret = 'T45Ag1sjaCDnB5kPCd8CahCzciAuVXIvmWhH0gIq';

        $adapter = new GamificationAdapter($this->baseUrl);
        $adapter->setClientId($clientId);
        $adapter->setClientSecret($clientSecret);
        $this->adapter = $adapter;
    }

    public function testContentLikeSuccess()
    {
        $content = new Content($this->adapter);
        $result = $content->like($this->cpf);
        $this->assertTrue($result);
    }

    public function testContentViewSuccess()
    {
        $content = new Content($this->adapter);
        $result = $content->view($this->cpf);
        $this->assertTrue($result);
    }

    public function testContentWatchVideoSuccess()
    {
        $content = new Content($this->adapter);
        $result = $content->watchVideo($this->cpf);
        $this->assertTrue($result);
    }

    public function testContentPublishSuccess()
    {
        $content = new Content($this->adapter);
        $result = $content->publish($this->cpf);
        $this->assertTrue($result);
    }

    public function testContentLikeError()
    {
        $content = new Content($this->adapter);
        $result = $content->likeNews($this->cpf);
        $this->assertFalse($result);
    }
}
