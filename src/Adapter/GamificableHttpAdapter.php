<?php

namespace Reciclare\Adapter;

interface GamificableHttpAdapter
{
    public function setUrl(string $url);

    public function getUrl();

    public function getClientId(): string;

    public function getClientSecret(): string;
}
