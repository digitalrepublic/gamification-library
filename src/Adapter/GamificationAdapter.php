<?php

namespace Reciclare\Adapter;

use Reciclare\Adapter\GamificableHttpAdapter;

class GamificationAdapter implements GamificableHttpAdapter
{
    public $url;
    public $http;
    public $clientId;
    public $clientSecret;

    public function __construct(string $url)
    {
        $this->setUrl($url);
    }

    /**
     * Retorna o url da api do gamification
     * @return void
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Setando url da api do gamification
     * @param string $url
     * @return void
     */
    public function setUrl(string $url)
    {
        $this->url = $url;
    }

    /**
     * Retornando o clientId
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * Setando o clientId que deve estar no arquivo de configuração
     * @param [type] $clientId
     * @return void
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * Retornando o clientSecret que é pego através do arquivo de configuração
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * Setando o clientSecret que dev estar no arquivo de configuração
     * @param [type] $clientSecret
     * @return void
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }
}
