<?php

use Reciclare\Adapter\GamificationAdapter;
use Reciclare\Gamification\Training;

class TrainingTest extends PHPUnit_Framework_TestCase
{
    protected $accessToken;
    protected $baseUrl = 'http://localhost:8000';
    protected $adapter;
    protected $cpf;

    public function setUp()
    {
        $this->setUpAdapter();
        $this->cpf = '45487975809';
    }

    public function setUpAdapter()
    {
        $clientId = 2;
        $clientSecret = 'T45Ag1sjaCDnB5kPCd8CahCzciAuVXIvmWhH0gIq';

        $adapter = new GamificationAdapter($this->baseUrl);
        $adapter->setClientId($clientId);
        $adapter->setClientSecret($clientSecret);
        $this->adapter = $adapter;
    }

    public function testTrainingStartSuccess()
    {
        $quiz = new Training($this->adapter);
        $result = $quiz->start($this->cpf);
        $this->assertTrue($result);
    }

    public function testTrainingEndSuccess()
    {
        $quiz = new Training($this->adapter);
        $result = $quiz->end($this->cpf);
        $this->assertTrue($result);
    }

    public function testTrainingStartError()
    {
        $quiz = new Training($this->adapter);
        $result = $quiz->startTraining($this->cpf);
        $this->assertFalse($result);
    }
}
